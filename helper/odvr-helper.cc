/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 IITP RAS
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Pavel Boyko <boyko@iitp.ru>, written after OlsrHelper by Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */
#include "odvr-helper.h"
#include "ns3/odvr-routing-protocol.h"
#include "ns3/node-list.h"
#include "ns3/names.h"
#include "ns3/ptr.h"
#include "ns3/ipv4-list-routing.h"

namespace ns3
{

OdvrHelper::OdvrHelper() :
  Ipv4RoutingHelper ()
{
  m_agentFactory.SetTypeId ("ns3::odvr::RoutingProtocol");
}

OdvrHelper*
OdvrHelper::Copy (void) const
{
  return new OdvrHelper (*this);
}

Ptr<Ipv4RoutingProtocol> 
OdvrHelper::Create (Ptr<Node> node) const
{
  Ptr<odvr::RoutingProtocol> agent = m_agentFactory.Create<odvr::RoutingProtocol> ();
  node->AggregateObject (agent);
  return agent;
}

void 
OdvrHelper::Set (std::string name, const AttributeValue &value)
{
  m_agentFactory.Set (name, value);
}

int64_t
OdvrHelper::AssignStreams (NodeContainer c, int64_t stream)
{
  int64_t currentStream = stream;
  Ptr<Node> node;
  for (NodeContainer::Iterator i = c.Begin (); i != c.End (); ++i)
    {
      node = (*i);
      Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
      NS_ASSERT_MSG (ipv4, "Ipv4 not installed on node");
      Ptr<Ipv4RoutingProtocol> proto = ipv4->GetRoutingProtocol ();
      NS_ASSERT_MSG (proto, "Ipv4 routing not installed on node");
      Ptr<odvr::RoutingProtocol> odvr = DynamicCast<odvr::RoutingProtocol> (proto);
      if (odvr)
        {
          currentStream += odvr->AssignStreams (currentStream);
          continue;
        }
      // Odvr may also be in a list
      Ptr<Ipv4ListRouting> list = DynamicCast<Ipv4ListRouting> (proto);
      if (list)
        {
          int16_t priority;
          Ptr<Ipv4RoutingProtocol> listProto;
          Ptr<odvr::RoutingProtocol> listOdvr;
          for (uint32_t i = 0; i < list->GetNRoutingProtocols (); i++)
            {
              listProto = list->GetRoutingProtocol (i, priority);
              listOdvr = DynamicCast<odvr::RoutingProtocol> (listProto);
              if (listOdvr)
                {
                  currentStream += listOdvr->AssignStreams (currentStream);
                  break;
                }
            }
        }
    }
  return (currentStream - stream);
}

int32_t
OdvrHelper::GetRreqRec(NodeContainer c){
    Ptr<Node> node;
    int cnt=0;
    for (NodeContainer::Iterator i = c.Begin (); i != c.End (); ++i)
    {

        node = (*i);
        Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
        // std::cout<< "Ipv4 not installed on node\n";
        Ptr<Ipv4RoutingProtocol> proto = ipv4->GetRoutingProtocol ();
        // std::cout<< "Ipv4 routing not installed on node\n";

        Ptr<odvr::RoutingProtocol> odvr = DynamicCast<odvr::RoutingProtocol> (proto);

        if(odvr){
            cnt+=odvr->GetRREQRec();
            continue;
        }

        Ptr<Ipv4ListRouting> list = DynamicCast<Ipv4ListRouting> (proto);
        if (list)
        {
            int16_t priority;
            Ptr<Ipv4RoutingProtocol> listProto;
            Ptr<odvr::RoutingProtocol> listOdvr;
            for (uint32_t i = 0; i < list->GetNRoutingProtocols (); i++)
            {
                listProto = list->GetRoutingProtocol (i, priority);
                listOdvr = DynamicCast<odvr::RoutingProtocol> (listProto);
                if (listOdvr)
                {
                    cnt+=listOdvr->GetRREQRec();
	            	  	    	  continue;
                }
            }
        }


    }

    return cnt;
}

int32_t
OdvrHelper::GetRreqGen(NodeContainer c){
    Ptr<Node> node;
    int cnt=0;
    for (NodeContainer::Iterator i = c.Begin (); i != c.End (); ++i)
    {

        node = (*i);
        Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
        // std::cout<< "Ipv4 not installed on node\n";
        Ptr<Ipv4RoutingProtocol> proto = ipv4->GetRoutingProtocol ();
        // std::cout<< "Ipv4 routing not installed on node\n";

        Ptr<odvr::RoutingProtocol> odvr = DynamicCast<odvr::RoutingProtocol> (proto);

        if(odvr){
            cnt+=odvr->GetRREQGen();
            continue;
        }

        Ptr<Ipv4ListRouting> list = DynamicCast<Ipv4ListRouting> (proto);
        if (list)
        {
            int16_t priority;
            Ptr<Ipv4RoutingProtocol> listProto;
            Ptr<odvr::RoutingProtocol> listOdvr;
            for (uint32_t i = 0; i < list->GetNRoutingProtocols (); i++)
            {
                listProto = list->GetRoutingProtocol (i, priority);
                listOdvr = DynamicCast<odvr::RoutingProtocol> (listProto);
                if (listOdvr)
                {
                    cnt+=listOdvr->GetRREQGen();
	            	  	    	  continue;
                }
            }
        }


    }

    return cnt;
}

int32_t
OdvrHelper::GetRrepRec(NodeContainer c){
    Ptr<Node> node;
    int cnt=0;
    for (NodeContainer::Iterator i = c.Begin (); i != c.End (); ++i)
    {
        node = (*i);
        Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
        NS_ASSERT_MSG (ipv4, "Ipv4 not installed on node");
        Ptr<Ipv4RoutingProtocol> proto = ipv4->GetRoutingProtocol ();
        NS_ASSERT_MSG (proto, "Ipv4 routing not installed on node");
        Ptr<odvr::RoutingProtocol> odvr = DynamicCast<odvr::RoutingProtocol> (proto);


        if(odvr){
            cnt+=odvr->GetRREPRec();
            continue;
        }

        Ptr<Ipv4ListRouting> list = DynamicCast<Ipv4ListRouting> (proto);
        if (list)
        {
            int16_t priority;
            Ptr<Ipv4RoutingProtocol> listProto;
            Ptr<odvr::RoutingProtocol> listOdvr;
            for (uint32_t i = 0; i < list->GetNRoutingProtocols (); i++)
            {
                listProto = list->GetRoutingProtocol (i, priority);
                listOdvr = DynamicCast<odvr::RoutingProtocol> (listProto);
                if (listOdvr)
                {
                    cnt+=listOdvr->GetRREPRec();
                    continue;
                }
            }
        }
    }
    return cnt;
}

int32_t
OdvrHelper::GetHelloRec(NodeContainer c){
    Ptr<Node> node;
    int cnt=0;
    for (NodeContainer::Iterator i = c.Begin (); i != c.End (); ++i)
    {
        node = (*i);
        Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
        NS_ASSERT_MSG (ipv4, "Ipv4 not installed on node");
        Ptr<Ipv4RoutingProtocol> proto = ipv4->GetRoutingProtocol ();
        NS_ASSERT_MSG (proto, "Ipv4 routing not installed on node");
        Ptr<odvr::RoutingProtocol> odvr = DynamicCast<odvr::RoutingProtocol> (proto);


        if(odvr){
            cnt+=odvr->GetHelloRec();
            continue;
        }

        Ptr<Ipv4ListRouting> list = DynamicCast<Ipv4ListRouting> (proto);
        if (list)
        {
            int16_t priority;
            Ptr<Ipv4RoutingProtocol> listProto;
            Ptr<odvr::RoutingProtocol> listOdvr;
            for (uint32_t i = 0; i < list->GetNRoutingProtocols (); i++)
            {
                listProto = list->GetRoutingProtocol (i, priority);
                listOdvr = DynamicCast<odvr::RoutingProtocol> (listProto);
                if (listOdvr)
                {
                    cnt+=listOdvr->GetHelloRec();
                    continue;
                }
            }
        }
    }
    return cnt;
}

int32_t
OdvrHelper::GetRerrRec(NodeContainer c){
    Ptr<Node> node;
    int cnt=0;
    for (NodeContainer::Iterator i = c.Begin (); i != c.End (); ++i)
    {
        node = (*i);
        Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
        NS_ASSERT_MSG (ipv4, "Ipv4 not installed on node");
        Ptr<Ipv4RoutingProtocol> proto = ipv4->GetRoutingProtocol ();
        NS_ASSERT_MSG (proto, "Ipv4 routing not installed on node");
        Ptr<odvr::RoutingProtocol> odvr = DynamicCast<odvr::RoutingProtocol> (proto);

        if(odvr){
            cnt+=odvr->GetRERRRec();
            continue;
        }

        Ptr<Ipv4ListRouting> list = DynamicCast<Ipv4ListRouting> (proto);
        if (list)
        {
            int16_t priority;
            Ptr<Ipv4RoutingProtocol> listProto;
            Ptr<odvr::RoutingProtocol> listOdvr;
            for (uint32_t i = 0; i < list->GetNRoutingProtocols (); i++)
            {
                listProto = list->GetRoutingProtocol (i, priority);
                listOdvr = DynamicCast<odvr::RoutingProtocol> (listProto);
                if (listOdvr)
                {
                    cnt+=listOdvr->GetRERRRec();
                    continue;
                }
            }
        }
    }

    return cnt;
}

int32_t
OdvrHelper::GetRrepAck(NodeContainer c){
    Ptr<Node> node;
    int cnt=0;
    for (NodeContainer::Iterator i = c.Begin (); i != c.End (); ++i)
    {
        node = (*i);
        Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
        NS_ASSERT_MSG (ipv4, "Ipv4 not installed on node");
        Ptr<Ipv4RoutingProtocol> proto = ipv4->GetRoutingProtocol ();
        NS_ASSERT_MSG (proto, "Ipv4 routing not installed on node");
        Ptr<odvr::RoutingProtocol> odvr = DynamicCast<odvr::RoutingProtocol> (proto);

        if(odvr){
            cnt+=odvr->GetRrepAck();
            continue;
        }

        Ptr<Ipv4ListRouting> list = DynamicCast<Ipv4ListRouting> (proto);
        if (list)
        {
            int16_t priority;
            Ptr<Ipv4RoutingProtocol> listProto;
            Ptr<odvr::RoutingProtocol> listOdvr;
            for (uint32_t i = 0; i < list->GetNRoutingProtocols (); i++)
            {
                listProto = list->GetRoutingProtocol (i, priority);
                listOdvr = DynamicCast<odvr::RoutingProtocol> (listProto);
                if (listOdvr)
                {
                    cnt+=listOdvr->GetRrepAck();
                    continue;
                }
            }
        }
    }

    return cnt;
}


int32_t
OdvrHelper::GetODVRs(NodeContainer c){
    Ptr<Node> node;
    int cnt=0;
    for (NodeContainer::Iterator i = c.Begin (); i != c.End (); ++i)
    {
        node = (*i);
        Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
        NS_ASSERT_MSG (ipv4, "Ipv4 not installed on node");
        Ptr<Ipv4RoutingProtocol> proto = ipv4->GetRoutingProtocol ();
        NS_ASSERT_MSG (proto, "Ipv4 routing not installed on node");
        Ptr<odvr::RoutingProtocol> odvr = DynamicCast<odvr::RoutingProtocol> (proto);

        if(odvr){
            cnt+=odvr->GetODVRs();
            continue;
        }

        Ptr<Ipv4ListRouting> list = DynamicCast<Ipv4ListRouting> (proto);
        if (list)
        {
            int16_t priority;
            Ptr<Ipv4RoutingProtocol> listProto;
            Ptr<odvr::RoutingProtocol> listOdvr;
            for (uint32_t i = 0; i < list->GetNRoutingProtocols (); i++)
            {
                listProto = list->GetRoutingProtocol (i, priority);
                listOdvr = DynamicCast<odvr::RoutingProtocol> (listProto);
                if (listOdvr)
                {
                    cnt+=listOdvr->GetODVRs();
                    continue;
                }
            }
        }
    }

    return cnt;
}

int32_t
OdvrHelper::GetRrepSent(NodeContainer c){
    Ptr<Node> node;
    int cnt=0;
    for (NodeContainer::Iterator i = c.Begin (); i != c.End (); ++i)
    {
        node = (*i);
        Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
        NS_ASSERT_MSG (ipv4, "Ipv4 not installed on node");
        Ptr<Ipv4RoutingProtocol> proto = ipv4->GetRoutingProtocol ();
        NS_ASSERT_MSG (proto, "Ipv4 routing not installed on node");
        Ptr<odvr::RoutingProtocol> odvr = DynamicCast<odvr::RoutingProtocol> (proto);

        if(odvr){
            cnt+=odvr->GetRREPSent();
            continue;
        }

        Ptr<Ipv4ListRouting> list = DynamicCast<Ipv4ListRouting> (proto);
        if (list)
        {
            int16_t priority;
            Ptr<Ipv4RoutingProtocol> listProto;
            Ptr<odvr::RoutingProtocol> listOdvr;
            for (uint32_t i = 0; i < list->GetNRoutingProtocols (); i++)
            {
                listProto = list->GetRoutingProtocol (i, priority);
                listOdvr = DynamicCast<odvr::RoutingProtocol> (listProto);
                if (listOdvr)
                {
                    cnt+=listOdvr->GetRREPSent();
                    continue;
                }
            }
        }
    }

    return cnt;
}

int32_t
OdvrHelper::GetRreqSent(NodeContainer c){
    Ptr<Node> node;
    int cnt=0;
    for (NodeContainer::Iterator i = c.Begin (); i != c.End (); ++i)
    {
        node = (*i);
        Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
        NS_ASSERT_MSG (ipv4, "Ipv4 not installed on node");
        Ptr<Ipv4RoutingProtocol> proto = ipv4->GetRoutingProtocol ();
        NS_ASSERT_MSG (proto, "Ipv4 routing not installed on node");
        Ptr<odvr::RoutingProtocol> odvr = DynamicCast<odvr::RoutingProtocol> (proto);

        if(odvr){
            cnt+=odvr->GetRREQSent();
            continue;
        }

        Ptr<Ipv4ListRouting> list = DynamicCast<Ipv4ListRouting> (proto);
        if (list)
        {
            int16_t priority;
            Ptr<Ipv4RoutingProtocol> listProto;
            Ptr<odvr::RoutingProtocol> listOdvr;
            for (uint32_t i = 0; i < list->GetNRoutingProtocols (); i++)
            {
                listProto = list->GetRoutingProtocol (i, priority);
                listOdvr = DynamicCast<odvr::RoutingProtocol> (listProto);
                if (listOdvr)
                {
                    cnt+=listOdvr->GetRREQSent();
                    continue;
                }
            }
        }
    }

    return cnt;
}

int32_t
OdvrHelper::GetRerrSent(NodeContainer c){
    Ptr<Node> node;
    int cnt=0;
    for (NodeContainer::Iterator i = c.Begin (); i != c.End (); ++i)
    {
        node = (*i);
        Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
        NS_ASSERT_MSG (ipv4, "Ipv4 not installed on node");
        Ptr<Ipv4RoutingProtocol> proto = ipv4->GetRoutingProtocol ();
        NS_ASSERT_MSG (proto, "Ipv4 routing not installed on node");
        Ptr<odvr::RoutingProtocol> odvr = DynamicCast<odvr::RoutingProtocol> (proto);

        if(odvr){
            cnt+=odvr->GetRERRSent();
            continue;
        }

        Ptr<Ipv4ListRouting> list = DynamicCast<Ipv4ListRouting> (proto);
        if (list)
        {
            int16_t priority;
            Ptr<Ipv4RoutingProtocol> listProto;
            Ptr<odvr::RoutingProtocol> listOdvr;
            for (uint32_t i = 0; i < list->GetNRoutingProtocols (); i++)
            {
                listProto = list->GetRoutingProtocol (i, priority);
                listOdvr = DynamicCast<odvr::RoutingProtocol> (listProto);
                if (listOdvr)
                {
                    cnt+=listOdvr->GetRERRSent();
                    continue;
                }
            }
        }
    }

    return cnt;
}

int32_t
OdvrHelper::GetRrepAckSent(NodeContainer c){
    Ptr<Node> node;
    int cnt=0;
    for (NodeContainer::Iterator i = c.Begin (); i != c.End (); ++i)
    {
        node = (*i);
        Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
        NS_ASSERT_MSG (ipv4, "Ipv4 not installed on node");
        Ptr<Ipv4RoutingProtocol> proto = ipv4->GetRoutingProtocol ();
        NS_ASSERT_MSG (proto, "Ipv4 routing not installed on node");
        Ptr<odvr::RoutingProtocol> odvr = DynamicCast<odvr::RoutingProtocol> (proto);

        if(odvr){
            cnt+=odvr->GetRREPACKSent();
            continue;
        }

        Ptr<Ipv4ListRouting> list = DynamicCast<Ipv4ListRouting> (proto);
        if (list)
        {
            int16_t priority;
            Ptr<Ipv4RoutingProtocol> listProto;
            Ptr<odvr::RoutingProtocol> listOdvr;
            for (uint32_t i = 0; i < list->GetNRoutingProtocols (); i++)
            {
                listProto = list->GetRoutingProtocol (i, priority);
                listOdvr = DynamicCast<odvr::RoutingProtocol> (listProto);
                if (listOdvr)
                {
                    cnt+=listOdvr->GetRREPACKSent();
                    continue;
                }
            }
        }
    }

    return cnt;
}

int32_t
OdvrHelper::GetHelloSent(NodeContainer c){
    Ptr<Node> node;
    int cnt=0;
    for (NodeContainer::Iterator i = c.Begin (); i != c.End (); ++i)
    {
        node = (*i);
        Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
        NS_ASSERT_MSG (ipv4, "Ipv4 not installed on node");
        Ptr<Ipv4RoutingProtocol> proto = ipv4->GetRoutingProtocol ();
        NS_ASSERT_MSG (proto, "Ipv4 routing not installed on node");
        Ptr<odvr::RoutingProtocol> odvr = DynamicCast<odvr::RoutingProtocol> (proto);

        if(odvr){
            cnt+=odvr->GetHELLOSent();
            continue;
        }

        Ptr<Ipv4ListRouting> list = DynamicCast<Ipv4ListRouting> (proto);
        if (list)
        {
            int16_t priority;
            Ptr<Ipv4RoutingProtocol> listProto;
            Ptr<odvr::RoutingProtocol> listOdvr;
            for (uint32_t i = 0; i < list->GetNRoutingProtocols (); i++)
            {
                listProto = list->GetRoutingProtocol (i, priority);
                listOdvr = DynamicCast<odvr::RoutingProtocol> (listProto);
                if (listOdvr)
                {
                    cnt+=listOdvr->GetHELLOSent();
                    continue;
                }
            }
        }
    }

    return cnt;
}

int32_t
  OdvrHelper::GetSigSize(NodeContainer c){
      Ptr<Node> node;
      int cnt=0;
      for (NodeContainer::Iterator i = c.Begin (); i != c.End (); ++i)
      {
          node = (*i);
          Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
          NS_ASSERT_MSG (ipv4, "Ipv4 not installed on node");
          Ptr<Ipv4RoutingProtocol> proto = ipv4->GetRoutingProtocol ();
          NS_ASSERT_MSG (proto, "Ipv4 routing not installed on node");
          Ptr<odvr::RoutingProtocol> odvr = DynamicCast<odvr::RoutingProtocol> (proto);

          if(odvr){
              cnt+=odvr->GetSigSize();
              continue;
          }

          Ptr<Ipv4ListRouting> list = DynamicCast<Ipv4ListRouting> (proto);
          if (list)
          {
              int16_t priority;
              Ptr<Ipv4RoutingProtocol> listProto;
              Ptr<odvr::RoutingProtocol> listOdvr;
              for (uint32_t i = 0; i < list->GetNRoutingProtocols (); i++)
              {
                  listProto = list->GetRoutingProtocol (i, priority);
                  listOdvr = DynamicCast<odvr::RoutingProtocol> (listProto);
                  if (listOdvr)
                  {
                      cnt+=listOdvr->GetSigSize();
                      continue;
                  }
              }
          }
      }

      return cnt;
  }


}
