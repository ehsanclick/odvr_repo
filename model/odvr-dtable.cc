/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2018 UC Santa Cruz
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 * Author: Ehsan Hemmati <ehsan@ehsan.click>
 * 			 <ehemmati@ucsc.edu>
 *
 */

#include "odvr-dtable.h"
#include <algorithm>
#include <iomanip>
#include "ns3/simulator.h"
#include "ns3/log.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("OdvrDistanceTable");

namespace odvr {

/// Distance Neighbor Entry

DistanceNeighborEntry::DistanceNeighborEntry (uint8_t distance, Time lifeTime)
  :
      m_distance (distance),
      m_lifeTime (lifeTime + Simulator::Now ())
{
}

DistanceNeighborEntry::~DistanceNeighborEntry ()
{
}

///
DistanceTableEntry::DistanceTableEntry (Ptr<NetDevice> dev, Ipv4InterfaceAddress iface, Time lifetime)
  :
      m_device (dev),
      m_iface (iface),
      m_lifeTime (lifetime + Simulator::Now ()),
      m_valid (true)
{
}
/*
 *
 */
DistanceTableEntry::~DistanceTableEntry ()
{
}
/**
 *
 */
bool
DistanceTableEntry::AddNeighborEntry (Ipv4Address dst, DistanceNeighborEntry & dne)
{
  NS_LOG_FUNCTION (this);
  DistanceNeighborEntry tmp;
  std::map<Ipv4Address, DistanceNeighborEntry>::iterator it = m_ipv4AddressEntry.find (dst);
  if (it != m_ipv4AddressEntry.end ())
    {
      if (!(it->second.GetLifeTime () < Seconds (0)))
	{
	  return false;
	}
      else
	{
	  m_ipv4AddressEntry.erase(it);
	}
    }
  std::pair<std::map<Ipv4Address, DistanceNeighborEntry>::iterator, bool> result =
    m_ipv4AddressEntry.insert (std::make_pair (dst, dne));
  return result.second;
}

bool
DistanceTableEntry::RemoveDest (Ipv4Address dst)
{
  NS_LOG_FUNCTION (this << dst);
  if (m_ipv4AddressEntry.erase (dst) != 0)
    {
      NS_LOG_LOGIC ("Route deletion to " << dst << " successful");
      return true;
    }
  NS_LOG_LOGIC ("Route deletion to " << dst << " not successful");
  return false;
}

bool
DistanceTableEntry::LookupEntry (Ipv4Address dst, DistanceNeighborEntry & dne)
{
  NS_LOG_FUNCTION (this << dst);
//  Purge ();
  if (m_ipv4AddressEntry.empty ())
    {
      NS_LOG_LOGIC ("DTE: Neighbor destination entry for " << dst << " not found; m_ipv4AddressEntry is empty");
      return false;
    }
  std::map<Ipv4Address, DistanceNeighborEntry>::iterator i =
    m_ipv4AddressEntry.find (dst);
  if (i == m_ipv4AddressEntry.end ())
    {
      NS_LOG_LOGIC ("DTE: Neighbor destination entry for " << dst << " not found");
      return false;
    }
  if (i->second.GetLifeTime () < Seconds (0))
    {
      m_ipv4AddressEntry.erase (i);
      return false;
    }
  dne = i->second;
  NS_LOG_LOGIC ("DTE: Neighbor destination entry for " << dst << " found");
  return true;
}

bool
DistanceTableEntry::LookupValidEntry (Ipv4Address dst, DistanceNeighborEntry & dne)
{
  //TODO Check the lifetime
  return LookupEntry (dst, dne);
}

bool
DistanceTableEntry::UpdateEntry (Ipv4Address dst, DistanceNeighborEntry & dne)
{
  NS_LOG_FUNCTION (this);
  std::map<Ipv4Address, DistanceNeighborEntry>::iterator i =
    m_ipv4AddressEntry.find (dst);
  if (i == m_ipv4AddressEntry.end ())
    {
      NS_LOG_LOGIC ("Lookup distanceTable for " << dst << " fails; not found");
      return false;
    }

  i->second = dne;
  return true;
}

void
DistanceTableEntry::Purge ()
{
  return;
  NS_LOG_FUNCTION (this);
  if (m_ipv4AddressEntry.empty ())
    {
      return;
    }
  for (std::map<Ipv4Address, DistanceNeighborEntry>::iterator i =
         m_ipv4AddressEntry.begin (); i != m_ipv4AddressEntry.end (); )
    {
      if (i->second.GetLifeTime () < Seconds (0))
        {

              std::map<Ipv4Address, DistanceNeighborEntry>::iterator tmp = i;
              ++i;
              m_ipv4AddressEntry.erase (tmp);
        }
      else
        {
          ++i;
        }
    }
}

 /*
 The Distance Table
 */
DistanceTable::DistanceTable (Time t)
  :
    m_lifetime (t + Simulator::Now ())
{
}
/**
 *
 */
bool
DistanceTable::AddEntry (Ipv4Address neighbor, DistanceTableEntry & dte)
{
  NS_LOG_FUNCTION (this);
  DistanceTableEntry tmp;
  std::map<Ipv4Address, DistanceTableEntry>::iterator it = m_ipv4AddressEntry.find (neighbor);
  if (it != m_ipv4AddressEntry.end ())
    {
      it->second.Purge ();
      if (it->second.Empty ())
	{
	  m_ipv4AddressEntry.erase (it);
	}
    }
  std::pair<std::map<Ipv4Address, DistanceTableEntry>::iterator, bool> result =
    m_ipv4AddressEntry.insert (std::make_pair (neighbor, dte));
  return result.second;
}
bool
DistanceTable::DeleteEntry (Ipv4Address neighbor)
{
  return m_ipv4AddressEntry.erase(neighbor) > 0;
}
/**
 *
 */
bool
DistanceTable::LookupEntry (Ipv4Address neighbor, DistanceTableEntry & dte)
{
  NS_LOG_FUNCTION (this << neighbor);
//  Purge ();
  if (m_ipv4AddressEntry.empty ())
    {
      NS_LOG_LOGIC ("Distance Table entry for " << neighbor << " not found; m_ipv4AddressEntry is empty");
      return false;
    }
  std::map<Ipv4Address, DistanceTableEntry>::iterator i =
    m_ipv4AddressEntry.find (neighbor);
  if (i == m_ipv4AddressEntry.end ())
    {
      NS_LOG_LOGIC ("distance table entry for " << neighbor << " not found");
      return false;
    }
  i->second.Purge ();
  if (i->second.Empty())
    {
      m_ipv4AddressEntry.erase (i);
      return false;
    }
  dte = i->second;
  NS_LOG_LOGIC ("DT: Neighbor entry for " << neighbor << " found");
  return true;
}
/**
 *
 */
bool
DistanceTable::LookupValidEntry (Ipv4Address neighbor, DistanceTableEntry & dte)
{
  return LookupEntry (neighbor, dte);
}
/**
 *
 */
bool
DistanceTable::UpdateEntry (Ipv4Address neighbor, DistanceTableEntry & dte)
{
  NS_LOG_FUNCTION (this);
  std::map<Ipv4Address, DistanceTableEntry>::iterator i =
    m_ipv4AddressEntry.find (neighbor);
  if (i == m_ipv4AddressEntry.end ())
    {
      NS_LOG_LOGIC ("Lookup distanceTable for " << neighbor << " fails; not found");
      return false;
    }

  i->second = dte;
  return true;
}
/**
 *
 */
void
DistanceTable::Purge ()
{
  return;
  for (std::map<Ipv4Address, DistanceTableEntry>::iterator it = m_ipv4AddressEntry.begin (); it != m_ipv4AddressEntry.end ();)
    {
      it->second.Purge ();
      if (it->second.Empty ())
	{
	  std::map<Ipv4Address, DistanceTableEntry>::iterator tmp = it;
	  ++it;
	  m_ipv4AddressEntry.erase (tmp);
	}
      else
	{
	  ++it;
	}
    }
}
/**
 *
 */
bool
DistanceTable::GetUpdateRoutingEntry (Ipv4Address dst, RoutingTableEntry & rt, Time lifeTime, bool  & changed)
{
  NS_LOG_FUNCTION (this << dst);
  Purge ();
  uint8_t dist = -1;
  std::map<Ipv4Address, DistanceTableEntry>::iterator nextHop = m_ipv4AddressEntry.end ();
  for (std::map<Ipv4Address, DistanceTableEntry>::iterator it = m_ipv4AddressEntry.begin (); it != m_ipv4AddressEntry.end (); it++)
    {
      DistanceNeighborEntry dne;
      if (it->second.LookupValidEntry(dst, dne))
	{
	  if (dne.GetDistance () < dist || (dne.GetDistance () == dist && it->first < nextHop->first))
	    {
	      dist = dne.GetDistance ();
	      nextHop = it;
	    }
	}
    }
  if (nextHop == m_ipv4AddressEntry.end ()) return false; //couldn't find next hop :(
  NS_LOG_DEBUG ("Found route to dest "  << dst << " dist: " << static_cast<unsigned>(dist + 1) << " nexthop: " <<  nextHop->first <<
      " iface: " << nextHop->second.GetInterface ());
  changed = rt.GetHop () != dist + 1;
  rt = RoutingTableEntry (/*device=*/ nextHop->second.GetOutputDevice () , /*dst=*/ dst, /*reqno=*/ rt.GetReqNo(),
                                           /*iface=*/ nextHop->second.GetInterface (), /*hops=*/ dist + 1, /*next hop=*/ nextHop->first,
					   /*lifetime=*/ lifeTime);
  return true;
}

void
DistanceTable::Print (Ptr<OutputStreamWrapper> stream) const
{
  *stream->GetStream () << "Print Not Implemented! \n";
}

} // odvr
} // ns3
