/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2018 UC Santa Cruz
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 * Author: Ehsan Hemmati <ehsan@ehsan.click>
 * 						 <ehemmati@ucsc.edu>
 *
 */
#ifndef ODVR_NTABLE_H
#define ODVR_NTABLE_H

#include <stdint.h>
#include <cassert>
#include <map>
#include <sys/types.h>
#include "ns3/ipv4.h"
#include "ns3/ipv4-route.h"
#include "ns3/timer.h"
#include "ns3/net-device.h"
#include "ns3/output-stream-wrapper.h"

namespace ns3 {
namespace odvr {

/**
 * \ingroup odvr
 * \brief neighbor states
 */
enum NeighborFlags
{
  REACHABLE = 0,          //!< REACHABLE
  UNREACHABLE = 1,        //!< UNREACHABLE
  UNIDIRECTED = 2,      //!< UNIDIRECTED
};

/**
 * \ingroup aodv
 * \brief Routing table entry
 */
class NeighborTableEntry
{
public:
  /**
   * constructor
   *
   * \param dev the device
   * \param dst the destination IP address
   * \param vSeqNo verify sequence number flag
   * \param seqNo the sequence number
   * \param iface the interface
   * \param hops the number of hops
   * \param nextHop the IP address of the next hop
   * \param lifetime the lifetime of the entry
   */
	NeighborTableEntry (Ipv4InterfaceAddress iface = Ipv4InterfaceAddress (),
						Ptr<NetDevice> dev = 0, Time lifetime = Simulator::Now ());

  ~NeighborTableEntry ();

  /**
   * Mark entry as "down" (i.e. disable it)
   * \param badLinkLifetime duration to keep entry marked as invalid
   */
  void Invalidate (Time badLinkLifetime);

  /**
   * Set output device
   * \param dev The output device
   */
  void SetOutputDevice (Ptr<NetDevice> dev)
  {
	m_outputDevice = dev;
  }
  /**
   * Get output device
   * \returns the output device
   */
  Ptr<NetDevice> GetOutputDevice () const
  {
    return m_outputDevice;
  }
  /**
   * Get the Ipv4InterfaceAddress
   * \returns the Ipv4InterfaceAddress
   */
  Ipv4InterfaceAddress GetInterface () const
  {
    return m_iface;
  }
  /**
   * Set the Ipv4InterfaceAddress
   * \param iface The Ipv4InterfaceAddress
   */
  void SetInterface (Ipv4InterfaceAddress iface)
  {
    m_iface = iface;
  }
  /**
   * Set the lifetime
   * \param lt The lifetime
   */
  void SetLifeTime (Time lt)
  {
    m_lifeTime = lt + Simulator::Now ();
  }
  /**
   * Get the lifetime
   * \returns the lifetime
   */
  Time GetLifeTime () const
  {
    return m_lifeTime - Simulator::Now ();
  }
  /**
   * Set the route flags
   * \param flag the route flags
   */
  void SetFlag (NeighborFlags flag)
  {
    m_flag = flag;
  }
  /**
   * Get the route flags
   * \returns the route flags
   */
  NeighborFlags GetFlag () const
  {
    return m_flag;
  }
  /**
   * Set the unidirectional flag
   * \param u the uni directional flag
   */
  void SetUnidirectional (bool u)
  {
    if (u) SetFlag (NeighborFlags::UNIDIRECTED);
  }
  /**
   * Get the unidirectional flag
   * \returns the unidirectional flag
   */
  bool IsUnidirectional () const
  {
    return GetFlag () == NeighborFlags::UNIDIRECTED;
  }
  bool operator== (NeighborTableEntry o) const
  {
    return (m_iface == o.m_iface);
  }
  /**
   * Print packet to trace file
   * \param stream The output stream
   */
  void Print (Ptr<OutputStreamWrapper> stream) const;

private:
  /**
  * \brief Expiration or deletion time of the route
  *	Lifetime field in the routing table plays dual role:
  *	for an active route it is the expiration time, and for an invalid route
  *	it is the deletion time.
  */
  Time m_lifeTime;
  /// Output device
  Ptr<NetDevice> m_outputDevice;
  /// Output interface address
  Ipv4InterfaceAddress m_iface;
  /// Routing flags: valid, invalid or in search
  NeighborFlags m_flag;
};

/**
 * \ingroup odvr
 * \brief The Neighbor table used by ODVR protocol
 *  Note: Neighbor Table is NOT defined in the paper
 *  It is only a mapping from neighbor IP address to its interface
 *  required by other tables!
 */
class NeighborTable
{
public:
  /**
   * constructor
   * \param t the routing table entry lifetime
   */
  NeighborTable (Time t);
  ///\name Handle lifetime of invalid route
  //\{
  Time GetBadLinkLifetime () const
  {
    return m_badLinkLifetime;
  }
  void SetBadLinkLifetime (Time t)
  {
    m_badLinkLifetime = t;
  }
  //\}
  /**
   * Add neighbor table entry if it doesn't yet exist in neighbor table
   * \param n neighbor table entry
   * \return true in success
   */
  bool AddNeighbor (NeighborTableEntry & nte);
  /**
   * Delete neighbor table entry with neighbor address neighbor, if it exists.
   * \param neighbor address
   * \return true on success
   */
  bool DeleteNeighbor (Ipv4Address neighbor);
  /**
   * Lookup neighbor table entry with neighbor address neighbor
   * \param neighbor  address
   * \param nte entry with neighbor address neighbor, if exists
   * \return true on success
   */
  bool LookupNeighbor (Ipv4Address neighbor, NeighborTableEntry & nte);
  /**
   * Lookup route in VALID state
   * \param neighbor destination address
   * \param nte entry with neighbor address neighbor, if exists
   * \return true on success
   */
  bool LookupValidNeighbor (Ipv4Address neighbor, NeighborTableEntry & nte);
  /**
   * Update neighbor table
   * \param neighbor entry with neighbor address neighbor, if exists
   * \return true on success
   */
  bool Update (NeighborTableEntry & nte);
  /**
   * Set routing table entry flags
   * \param dst destination address
   * \param state the routing flags
   * \return true on success
   */
  bool SetNeighborState (Ipv4Address neighbor, NeighborFlags state);
  /// Delete all outdated entries and invalidate valid entry if Lifetime is expired
  void Purge ();
  /** Mark entry as unidirectional (e.g. add this neighbor to "blacklist" for blacklistTimeout period)
   * \param neighbor - neighbor address link to which assumed to be unidirectional
   * \param blacklistTimeout - time for which the neighboring node is put into the blacklist
   * \return true on success
   */
  bool MarkLinkAsUnidirectional (Ipv4Address neighbor, Time blacklistTimeout);
  /**
   * Print routing table
   * \param stream the output stream
   */
  void Print (Ptr<OutputStreamWrapper> stream) const;

private:
  /// The neighbor table
  std::map<Ipv4Address, NeighborTableEntry> m_ipv4AddressEntry;
  /// Deletion time for invalid routes
  Time m_badLinkLifetime;
  /**
   * const version of Purge, for use by Print() method
   * \param table the routing table entry to purge
   */
  void Purge (std::map<Ipv4Address, NeighborTableEntry> &table) const;
};

} // odvr
} // ns3

#endif /* ODVR_NTABLE_H */
