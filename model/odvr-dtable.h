/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2018 UC Santa Cruz
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 * Author: Ehsan Hemmati <ehsan@ehsan.click>
 * 			 <ehemmati@ucsc.edu>
 *
 */
#ifndef ODVR_DTABLE_H
#define ODVR_DTABLE_H

#include <stdint.h>
#include <cassert>
#include <map>
#include <sys/types.h>
#include "ns3/ipv4.h"
#include "ns3/ipv4-route.h"
#include "ns3/timer.h"
#include "ns3/net-device.h"
#include "ns3/output-stream-wrapper.h"

#include "odvr-rtable.h"

namespace ns3 {
namespace odvr {

/**
 * \ingroup odvr
 * \brief Distance Neighbor entry
 */
class DistanceNeighborEntry
{
public:
  /**
   * constructor
   *
   * \param dev the device
   * \param dst the destination IP address
   * \param vSeqNo verify sequence number flag
   * \param seqNo the sequence number
   * \param iface the interface
   * \param hops the number of hops
   * \param nextHop the IP address of the next hop
   * \param lifetime the lifetime of the entry
  */
  DistanceNeighborEntry (uint8_t distance = 0, Time lifeTime = Simulator::Now ());

  ~DistanceNeighborEntry ();

  void SetDistance (uint8_t distance)
  {
    m_distance = distance;
  }

  uint8_t GetDistance ()
  {
    return m_distance;
  }
  /**
  * Set the lifetime
  * \param lt The lifetime
  */
  void SetLifeTime (Time lt)
  {
    m_lifeTime = lt + Simulator::Now ();
  }
  /**
  * Get the lifetime
  * \returns the lifetime
  */
  Time GetLifeTime () const
  {
    return m_lifeTime - Simulator::Now ();
  }
  void UpdateLifeTime (Time lt)
  {
    m_lifeTime = std::max (m_lifeTime, lt);
  }
  /**
  * Set the route flags
  * \param flag the route flags
  */
private:
   uint8_t m_distance;
   Time m_lifeTime;
};


/**
 * \ingroup odvr
 * \brief Distance table entry
 */
class DistanceTableEntry
{
public:
  /**
   * constructor
   *
   * \param dev the device
   * \param dst the destination IP address
   * \param vSeqNo verify sequence number flag
   * \param seqNo the sequence number
   * \param iface the interface
   * \param hops the number of hops
   * \param nextHop the IP address of the next hop
   * \param lifetime the lifetime of the entry
   */
  DistanceTableEntry (Ptr<NetDevice> dev = 0, Ipv4InterfaceAddress iface = Ipv4InterfaceAddress (), Time lifetime = Simulator::Now ());

  ~DistanceTableEntry ();

  /**
   * Set output device
   * \param dev The output device
   */
  void SetOutputDevice (Ptr<NetDevice> dev)
  {
    m_device = dev;
  }
  /**
   * Get output device
   * \returns the output device
   */
  Ptr<NetDevice> GetOutputDevice () const
  {
    return m_device;
  }
  /**
   * Get the Ipv4InterfaceAddress
   * \returns the Ipv4InterfaceAddress
   */
  Ipv4InterfaceAddress GetInterface () const
  {
    return m_iface;
  }
  /**
   * Set the Ipv4InterfaceAddress
   * \param iface The Ipv4InterfaceAddress
   */
  void SetInterface (Ipv4InterfaceAddress iface)
  {
    m_iface = iface;
  }

  void SetValidFlag (bool valid = true)
  {
    m_valid = valid;
  }

  bool GetValidFlag ()
  {
    return m_valid;
  }

  void Invalidate ()
  {
    m_valid = false;
  }

  bool AddNeighborEntry (Ipv4Address dst, DistanceNeighborEntry & dne);

  bool RemoveDest (Ipv4Address dst);

  void Clear ()
  {
    m_ipv4AddressEntry.clear ();
  }

  bool Empty ()
  {
    return m_ipv4AddressEntry.empty ();
  }

  void UpdateLifeTime (Time lifetime)
  {
    m_lifeTime = std::max (m_lifeTime, lifetime);
    for (std::map<Ipv4Address, DistanceNeighborEntry>::iterator it = m_ipv4AddressEntry.begin ();
	it != m_ipv4AddressEntry.end (); it++)
      {
	it->second.UpdateLifeTime (lifetime);
      }
  }

  /**
   * Lookup routing table entry with destination address dst
   * \param dst destination address
   * \param rt entry with destination address dst, if exists
   * \return true on success
   */
  bool LookupEntry (Ipv4Address dst, DistanceNeighborEntry & dte);
  /**
   * Lookup route in VALID state
   * \param dst destination address
   * \param rt entry with destination address dst, if exists
   * \return true on success
   */
  bool LookupValidEntry (Ipv4Address dst, DistanceNeighborEntry & dne);
  /**
   *
   */
  bool UpdateEntry (Ipv4Address dst, DistanceNeighborEntry & dne);
  /**
   *
   */
  void Purge ();
  /**
   * \brief Compare destination address
   * \param dst IP address to compare
   * \return true if equal
   */
  bool operator== (DistanceTableEntry const  entry) const
  {
    return (m_device == entry.m_device && m_iface == entry.m_iface);
  }

private:

  Ptr<NetDevice> m_device;
  /// Output interface address
  Ipv4InterfaceAddress m_iface;
  /**
  * \brief Expiration or deletion time of the route
  *	Lifetime field in the routing table plays dual role:
  *	for an active route it is the expiration time, and for an invalid route
  *	it is the deletion time.
  */
  Time m_lifeTime;
  /**
   * const version of Purge, for use by Print() method
   * \param table the routing table entry to purge
   */
  std::map<Ipv4Address, DistanceNeighborEntry> m_ipv4AddressEntry; // dest -> distance
  /// Valid
  bool m_valid;
};

/**
 * \ingroup odvr
 * \brief The Distance table used by ODVR protocol
 */
class DistanceTable
{
public:
  /**
   * constructor
   * \param t the distance table entry lifetime
   */
  DistanceTable (Time t);
  ///\name Handle lifetime of invalid route
  //\{
  Time GetLifetime () const
  {
    return m_lifetime - Simulator::Now ();
  }
  void SetBadLinkLifetime (Time t)
  {
    m_lifetime = t;
  }
  //\}
  /**
   * Add routing table entry if it doesn't yet exist in routing table
   * \param r routing table entry
   * \return true in success
   */
  bool AddEntry (Ipv4Address neighbor, DistanceTableEntry & dte);
  /**
   * Delete routing table entry with destination address dst, if it exists.
   * \param dst destination address
   * \return true on success
   */
  bool DeleteEntry (Ipv4Address dst);
  /**
   * Lookup routing table entry with destination address dst
   * \param dst destination address
   * \param rt entry with destination address dst, if exists
   * \return true on success
   */
  bool LookupEntry (Ipv4Address nrighbor, DistanceTableEntry & dte);
  /**
   * Lookup route in VALID state
   * \param dst destination address
   * \param rt entry with destination address dst, if exists
   * \return true on success
   */
  bool LookupValidEntry (Ipv4Address neighbor, DistanceTableEntry & dte);
  /**
   * Update routing table
   * \param rt entry with destination address dst, if exists
   * \return true on success
   */
  bool UpdateEntry (Ipv4Address dst, DistanceTableEntry & dte);
  /// Delete all entries from routing table
  //void Clear ();
  /// Delete all outdated entries and invalidate valid entry if Lifetime is expired
  void Purge ();
  /** Mark entry as unidirectional (e.g. add this neighbor to "blacklist" for blacklistTimeout period)
   * \param neighbor - neighbor address link to which assumed to be unidirectional
   * \param blacklistTimeout - time for which the neighboring node is put into the blacklist
   * \return true on success
   */
  //bool MarkLinkAsUnidirectional (Ipv4Address neighbor, Time blacklistTimeout);
  /**
   * Routing Table Entry based on current pending table
   */
  bool GetUpdateRoutingEntry (Ipv4Address dst, RoutingTableEntry & rt, Time lifeTime, bool & changed);
  /**
   * Print routing table
   * \param stream the output stream
   */
  void Print (Ptr<OutputStreamWrapper> stream) const;

private:
  /// The routing table
  std::map<Ipv4Address, DistanceTableEntry> m_ipv4AddressEntry; //neighbor -> Entry
  /// Deletion time for invalid routes
  Time m_lifetime;
  /**
   * const version of Purge, for use by Print() method
   * \param table the routing table entry to purge
   */
  void Purge (std::map<Ipv4Address, DistanceTableEntry> &table) const;
};

}  // namespace aodv
}  // namespace ns3


#endif /* ODVR_DTABLE_H_ */
