/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2018 UCSC
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 * Author: Ehsan Hemmati <ehsan@ehsan.click>
 * 			 <ehemmati@ucsc.edu>
 *
 */

#ifndef ODVR_PTABLE_H
#define ODVR_PTABLE_H

#include <stdint.h>
#include <cassert>
#include <map>
#include <sys/types.h>
#include "ns3/ipv4.h"
#include "ns3/ipv4-route.h"
#include "ns3/timer.h"
#include "ns3/net-device.h"
#include "ns3/output-stream-wrapper.h"

namespace ns3 {
namespace odvr {


struct PrnEntry
{
  uint8_t m_dist;
  uint8_t m_refDist;
  Time m_lifeTime;
  //Ptr<NetDevice> m_dev;
  //Ipv4InterfaceAddress m_iface;
  //Ipv4Address m_pred;

  PrnEntry (uint8_t dist = 0, uint8_t refDist = 0, Time lt = Simulator::Now ());

  void SetDistance (uint8_t dist)
  {
    m_dist = dist;
  }
  uint8_t GetDistance ()
  {
    return m_dist;
  }
  /**
   *
   */
  void SetRefDist (uint8_t refDist)
  {
    m_refDist = refDist;
  }
  uint8_t
  GetRefDist ()
  {
    return m_refDist;
  }
  /**
   * Set the lifetime
   * \param lt The lifetime
   */
  void SetLifeTime (Time lt)
  {
    m_lifeTime = lt + Simulator::Now ();
  }
  /**
   * Set the lifetime
   * \param lt The lifetime
   */
  void UpdateLifeTime (Time lt)
  {
    m_lifeTime = std::max(lt + Simulator::Now (), m_lifeTime);
  }
  /**
   * Get the lifetime
   * \returns the lifetime
   */
  Time GetLifeTime () const
  {
    return m_lifeTime - Simulator::Now ();
  }
};

/**
 * \ingroup odvr
 * \brief Request List entry (a.k.a. pending-request neighbors)
 */
class ReqListEntry
{
public:
  /**
   *
   */
  ReqListEntry ();
  /**
   *
   */
  ~ReqListEntry ();
  /**
   *
   */
  bool AddReqEntry (Ipv4Address neighbor, PrnEntry prnEntry);
  /*
   *
   *
   */
  bool LookupReqEntry (Ipv4Address neighbor, PrnEntry  & prnEntry);
  /**
   *
   */
  bool LookupValidReqEntry (Ipv4Address neighbor, PrnEntry & prnEntry);
  /**
   *
   */
  bool UpdateReqEntry (Ipv4Address neighbor, PrnEntry & prnEntry);
  /**
   *
   */
  bool Empty ()
  {
    return m_prn.empty ();
  }
  /**
   *
   */
  void Clear ()
  {
    m_prn.clear ();
  }
  //bool SetDistance (Ipv4Address neighbor, uint8_t dist);
  //uint8_t GetDistance (Ipv4Address neighbor);
  void Purge ();

private:
  std::map<Ipv4Address, PrnEntry> m_prn; //neighbor to distance
};

/**
 * \ingroup odvr
 * \brief pending table entry. aka Request List
 */
class PendingTableEntry
{
public:
  /**
   * constructor
   *
   * \param dev the device
   * \param dst the destination IP address
   * \param vSeqNo verify sequence number flag
   * \param seqNo the sequence number
   * \param iface the interface
   * \param hops the number of hops
   * \param nextHop the IP address of the next hop
   * \param lifetime the lifetime of the entry
   */
  PendingTableEntry (uint8_t refDist = 0, Time lifetime = Simulator::Now ());

  ~PendingTableEntry ();

  /**
   * Add routing table entry if it doesn't yet exist in routing table
   * \param r routing table entry
   * \return true in success
   */
  bool AddReqList (Ipv4Address org, ReqListEntry & r);
  /**
   * Delete routing table entry with destination address dst, if it exists.
   * \param dst destination address
   * \return true on success
   */
  //bool DeleteReqList (Ipv4Address s);
  /**
   *
   */
  bool Update (Ipv4Address org, ReqListEntry & r);
  /**
   * Lookup routing table entry with destination address dst
   * \param dst destination address
   * \param rt entry with destination address dst, if exists
   * \return true on success
   */
  bool LookupReqList (Ipv4Address org, ReqListEntry & pte);
  /**
   * Lookup route in VALID state
   * \param dst destination address
   * \param rt entry with destination address dst, if exists
   * \return true on success
   */
  bool LookupValidReqList (Ipv4Address org, ReqListEntry & pt);
  /**
   * Update routing table
   * \param rt entry with destination address dst, if exists
   * \return true on success
   */
  //bool Update (Ipv4Address org, ReqListEntry & pte);
  /**
   *
   *
   */
  bool DeleteReqList (Ipv4Address org);
  /**
   *
   */
  void SetRefDistance (uint8_t refDist)
  {
    m_refDist = refDist;
  }
  /**
   *
   */
  void SetLifeTime (Time lt)
  {
    m_lifeTime = lt + Simulator::Now ();
  }
  /**
   *
   */
  Time GetLifeTime ()
  {
    return m_lifeTime - Simulator::Now ();
  }
  /**
   *
   */
  void UpdateLifeTime (Time lt)
  {
    m_lifeTime = std::max (m_lifeTime, lt + Simulator::Now ());
  }
  /**
   *
   */
  uint8_t GetRefDist ()
  {
    return m_refDist;
  }
  /**
   * \brief Compare destination address
   * \param dst IP address to compare
   * \return true if equal
   */
  bool operator== (Ipv4Address const  dst) const
  {
    return (true); //\TODO
  }
  /**
   *
   */
  void Purge ();
  /**
   *
   */
  bool Empty ()
  {
    return m_requestList.empty ();
  }
  /**
   * Print packet to trace file
   * \param stream The output stream
   */
  void Print (Ptr<OutputStreamWrapper> stream) const;
  void Print (std::ostream &os) const;

private:
  /// Refrence Distanace
  uint8_t m_refDist;
  /// The Request List
  std::map<Ipv4Address, ReqListEntry> m_requestList; // aka RL_d: origin to pending request neighbors
  /**
  * \brief Expiration or deletion time of the route
  *	Lifetime field in the routing table plays dual role:
  *	for an active route it is the expiration time, and for an invalid route
  *	it is the deletion time.
  */
  Time m_lifeTime;

};

/**
 * \ingroup odvr
 * \brief The Routing table used by ODVR protocol
 */
class PendingTable
{
public:
  /**
   * constructor
   * \param t the routing table entry lifetime
   */
  PendingTable (Time t);
  ~PendingTable ();
  ///\name Handle lifetime of invalid route
  //\{
  Time GetBadLinkLifetime () const
  {
    return m_badLinkLifetime;
  }
  void SetBadLinkLifetime (Time t)
  {
    m_badLinkLifetime = t;
  }
  //\}
  /**
   * Add routing table entry if it doesn't yet exist in routing table
   * \param r routing table entry
   * \return true in success
   */
  bool AddEntry (Ipv4Address dst, PendingTableEntry & p);
  /**
   *
   */
  bool Update (Ipv4Address dst, PendingTableEntry & p);
  /**
   * Delete routing table entry with destination address dst, if it exists.
   * \param dst destination address
   * \return true on success
   */
  bool DeleteEntry (Ipv4Address dst, Ipv4Address orig);
  /**
   * Lookup routing table entry with destination address dst
   * \param dst destination address
   * \param rt entry with destination address dst, if exists
   * \return true on success
   */
  bool LookupEntry (Ipv4Address dst, PendingTableEntry & pte);
  /**
   * Lookup route in VALID state
   * \param dst destination address
   * \param rt entry with destination address dst, if exists
   * \return true on success
   */
  bool LookupValidEntry (Ipv4Address dst, PendingTableEntry & pt);
  /**
   * Update routing table
   * \param rt entry with destination address dst, if exists
   * \return true on success
   */
  //bool Update (PendingTableEntry & pte);
  /**
   * Print routing table
   * \param stream the output stream
   */
  void Print (Ptr<OutputStreamWrapper> stream) const;

private:
  /// The routing table
  std::map<Ipv4Address, PendingTableEntry> m_ipv4AddressEntry; // Destination to Request List
  /// Deletion time for invalid routes
  Time m_badLinkLifetime;
  /**
   * const version of Purge, for use by Print() method
   * \param table the routing table entry to purge
   */
  //void Purge (std::map<Ipv4Address, PendingTableEntry> &table) const;
};

std::ostream& operator<< (std::ostream& os, PendingTableEntry const& pte);


}  // namespace odvr
}  // namespace ns3

#endif /* ODVR_PTABLE_H */
