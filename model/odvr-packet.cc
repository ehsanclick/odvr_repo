/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2018 UC Santa Cruz
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 * Author: Ehsan Hemmati <ehsan@ehsan.click>
 * 						 <ehemmati@ucsc.edu>
 *
 */
#include "odvr-packet.h"
#include "ns3/address-utils.h"
#include "ns3/packet.h"

#define IPV4_ADDRESS_SIZE 4
#define ODVR_PKT_HEADER_SIZE 8 // 2 + 4
#define ODVR_ENTRY_HEADER_SIZE 1
#define ODVR_REQ_FIXEDHEADER_SIZE 22
#define ODVR_REP_HEADER_SIZE 17 // 1 + 1 + 1 + 1 + 1 + 4 + 4 + 4

namespace ns3 {
namespace odvr {

// ---------------- ODVR Packet Header -------------------------------

NS_OBJECT_ENSURE_REGISTERED (PacketHeader);

PacketHeader::PacketHeader ()
  :
      m_packetLength (0),
      m_reserved (0)
{
}

PacketHeader::~PacketHeader ()
{
}

TypeId
PacketHeader::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::odvr::PacketHeader")
    .SetParent<Header> ()
    .SetGroupName ("Odvr")
    .AddConstructor<PacketHeader> ()
  ;
  return tid;
}
TypeId
PacketHeader::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
PacketHeader::GetSerializedSize (void) const
{
  return ODVR_PKT_HEADER_SIZE;
}

void
PacketHeader::Print (std::ostream &os) const
{
  /// \todo
}

void
PacketHeader::Serialize (Buffer::Iterator start) const
{
  Buffer::Iterator i = start;
  i.WriteHtonU16 (m_packetLength);
}

uint32_t
PacketHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;
  m_packetLength  = i.ReadNtohU16 ();
  return GetSerializedSize ();
}


// ---------------- ODVR Entry Header -------------------------------

NS_OBJECT_ENSURE_REGISTERED (EntryHeader);

EntryHeader::EntryHeader (MessageType t)
  : m_type (t),
    m_valid (true)
{
}

TypeId
EntryHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::odvr::EntryHeader")
    .SetParent<Header> ()
    .SetGroupName ("Odvr")
    .AddConstructor<EntryHeader> ()
  ;
  return tid;
}

TypeId
EntryHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

uint32_t
EntryHeader::GetSerializedSize () const
{
  return ODVR_ENTRY_HEADER_SIZE;
}

void
EntryHeader::Serialize (Buffer::Iterator i) const
{
  i.WriteU8 ((uint8_t) m_type);
}

uint32_t
EntryHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;
  uint8_t type = i.ReadU8 ();
  m_valid = true;
  switch (type)
    {
    case ODVRTYPE_RREQ:
    case ODVRTYPE_RREP:
      {
        m_type = (MessageType) type;
        break;
      }
    default:
      m_valid = false;
    }
  uint32_t dist = i.GetDistanceFrom (start);
  NS_ASSERT (dist == GetSerializedSize ());
  return dist;
}

void
EntryHeader::Print (std::ostream &os) const
{
  switch (m_type)
    {
    case ODVRTYPE_RREQ:
      {
        os << "RREQ";
        break;
      }
    case ODVRTYPE_RREP:
      {
        os << "RREP";
        break;
      }
    default:
      os << "UNKNOWN_TYPE";
    }
}

bool
EntryHeader::operator== (EntryHeader const & o) const
{
  return (m_type == o.m_type && m_valid == o.m_valid);
}

std::ostream &
operator<< (std::ostream & os, EntryHeader const & h)
{
  h.Print (os);
  return os;
}

//-----------------------------------------------------------------------------
// RREQ
//-----------------------------------------------------------------------------
RreqHeader::RreqHeader (uint8_t flags, uint8_t reserved, uint8_t refDist,
	      uint8_t hopCount, uint32_t requestID,
		  Ipv4Address dst, Ipv4Address origin, Ipv4Address predecessor, std::vector<Ipv4Address> neighborList)
  : m_flags (flags),
    m_reserved (reserved),
	m_refDist (refDist),
    m_hopCount (hopCount),
    m_requestID (requestID),
    m_dst (dst),
    m_origin (origin),
	m_neighborAddresses (neighborList)
{
}

NS_OBJECT_ENSURE_REGISTERED (RreqHeader);

TypeId
RreqHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::odvr::RreqHeader")
    .SetParent<Header> ()
    .SetGroupName ("Odvr")
    .AddConstructor<RreqHeader> ()
  ;
  return tid;
}

TypeId
RreqHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

void
RreqHeader::AddNeighbor (Ipv4Address neighbor)
{
  for (std::vector<Ipv4Address>::iterator it = m_neighborAddresses.begin();
		  it != m_neighborAddresses.end(); it++){
	  if (*it == neighbor) return;
  }
  m_neighborAddresses.push_back(neighbor);
}

void
RreqHeader::SetTargeted (bool f)
{
  if (f)
    {
      m_flags |= (1 << 7);
    }
  else
    {
      m_flags &= ~(1 << 7);
    }
}

bool
RreqHeader::GetTargeted () const
{
  return (m_flags & (1 << 7));
}

void
RreqHeader::SetGratuitousRrep (bool f)
{
  if (f)
    {
      m_flags |= (1 << 5);
    }
  else
    {
      m_flags &= ~(1 << 5);
    }
}

bool
RreqHeader::GetGratuitousRrep () const
{
  return (m_flags & (1 << 5));
}

void
RreqHeader::SetDestinationOnly (bool f)
{
  if (f)
    {
      m_flags |= (1 << 4);
    }
  else
    {
      m_flags &= ~(1 << 4);
    }
}

bool
RreqHeader::GetDestinationOnly () const
{
  return (m_flags & (1 << 4));
}

uint32_t
RreqHeader::GetSerializedSize () const
{
  return ODVR_REQ_FIXEDHEADER_SIZE + (m_neighborAddresses.size() * 4);
}

void
RreqHeader::Serialize (Buffer::Iterator i) const
{
  i.WriteHtonU16(GetSerializedSize());
  i.WriteU8 (m_flags);
  i.WriteU8 (m_reserved);
  i.WriteU8 (m_refDist);
  i.WriteU8 (m_hopCount);
  i.WriteHtonU32 (m_requestID);
  WriteTo (i, m_dst);
  WriteTo (i, m_origin);
  WriteTo (i, m_predecessor);
  for (std::vector<Ipv4Address>::const_iterator iter = m_neighborAddresses.begin ();
       iter != m_neighborAddresses.end (); iter++)
    {
      WriteTo (i, *iter);
    }
}

uint32_t
RreqHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;
  uint16_t messageSize  = i.ReadNtohU16();
  m_flags = i.ReadU8 ();
  m_reserved = i.ReadU8 ();
  m_refDist = i.ReadU8 ();
  m_hopCount = i.ReadU8 ();
  m_requestID = i.ReadNtohU32 ();
  ReadFrom (i, m_dst);
  ReadFrom (i, m_origin);
  ReadFrom (i, m_predecessor);
  uint16_t numAddresses = (messageSize - ODVR_REQ_FIXEDHEADER_SIZE) / 4;
  m_neighborAddresses.clear ();
  Ipv4Address neighbor_addr;
  for (int n = 0; n < numAddresses; ++n){
     ReadFrom (i, neighbor_addr);
     m_neighborAddresses.push_back (neighbor_addr);
  }

  uint32_t dist = i.GetDistanceFrom (start);
  NS_ASSERT (dist == GetSerializedSize ());
  return dist;
}

void
RreqHeader::Print (std::ostream &os) const
{
  os << "RREQ ID " << m_requestID << " destination: ipv4 " << m_dst
     << " Reference distance: " << static_cast<uint32_t> (m_refDist) << " source: ipv4 "
     << m_origin << " neighbor size: " << m_neighborAddresses.size();
}

std::ostream &
operator<< (std::ostream & os, RreqHeader const & h)
{
  h.Print (os);
  return os;
}

bool
RreqHeader::operator== (RreqHeader const & o) const
{
  return (m_flags == o.m_flags && m_reserved == o.m_reserved
          && m_hopCount == o.m_hopCount && m_requestID == o.m_requestID
          && m_dst == o.m_dst && m_predecessor == o.m_predecessor
		  && m_origin == o.m_origin && m_refDist == o.m_refDist);
}

//-----------------------------------------------------------------------------
// RREP
//-----------------------------------------------------------------------------

RrepHeader::RrepHeader (uint8_t prefixSize, uint8_t refDist, uint8_t hopCount,
  	    				uint8_t distToOrigin, Ipv4Address dst, Ipv4Address origin,
						Time lifetime)
  : m_flags (0),
    m_prefixSize (prefixSize),
	m_refDist (refDist),
    m_hopCount (hopCount),
	m_distToOrigin (distToOrigin),
    m_dst (dst),
    m_origin (origin)
{
  m_lifeTime = uint32_t (lifetime.GetMilliSeconds ());
}

NS_OBJECT_ENSURE_REGISTERED (RrepHeader);

TypeId
RrepHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::odvr::RrepHeader")
    .SetParent<Header> ()
    .SetGroupName ("Odvr")
    .AddConstructor<RrepHeader> ()
  ;
  return tid;
}

TypeId
RrepHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

uint32_t
RrepHeader::GetSerializedSize () const
{
  return ODVR_REP_HEADER_SIZE;
}

void
RrepHeader::Serialize (Buffer::Iterator i) const
{
  i.WriteU8 (m_flags);
  i.WriteU8 (m_prefixSize);
  i.WriteU8 (m_refDist);
  i.WriteU8 (m_hopCount);
  i.WriteU8 (m_distToOrigin);
  WriteTo (i, m_dst);
  WriteTo (i, m_origin);
  i.WriteHtonU32 (m_lifeTime);
}

uint32_t
RrepHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;

  m_flags = i.ReadU8 ();
  m_prefixSize = i.ReadU8 ();
  m_refDist = i.ReadU8 ();
  m_hopCount = i.ReadU8 ();
  m_distToOrigin = i.ReadU8 ();
  ReadFrom (i, m_dst);
  ReadFrom (i, m_origin);
  m_lifeTime = i.ReadNtohU32 ();

  uint32_t dist = i.GetDistanceFrom (start);
  NS_ASSERT (dist == GetSerializedSize ());
  return dist;
}

void
RrepHeader::Print (std::ostream &os) const
{
  os << "destination: ipv4 " << m_dst << " reference distance: " << static_cast<uint32_t> (m_refDist)
		  << " distance: " << static_cast<uint32_t> (m_hopCount) << " distance to origin: " << static_cast<uint32_t> (m_distToOrigin);
  if (m_prefixSize != 0)
    {
      os << " prefix size " << m_prefixSize;
    }
  os << " source ipv4 " << m_origin << " lifetime " << m_lifeTime;
}

void
RrepHeader::SetLifeTime (Time t)
{
  m_lifeTime = t.GetMilliSeconds ();
}

Time
RrepHeader::GetLifeTime () const
{
  Time t (MilliSeconds (m_lifeTime));
  return t;
}

void
RrepHeader::SetAckRequired (bool f)
{
  if (f)
    {
      m_flags |= (1 << 6);
    }
  else
    {
      m_flags &= ~(1 << 6);
    }
}

bool
RrepHeader::GetAckRequired () const
{
  return (m_flags & (1 << 6));
}

void
RrepHeader::SetPrefixSize (uint8_t sz)
{
  m_prefixSize = sz;
}

uint8_t
RrepHeader::GetPrefixSize () const
{
  return m_prefixSize;
}

bool
RrepHeader::operator== (RrepHeader const & o) const
{
  return (m_flags == o.m_flags && m_prefixSize == o.m_prefixSize
          && m_refDist == o.m_refDist && m_hopCount == o.m_hopCount
		  && m_dst == o.m_dst && m_origin == o.m_origin
		  && m_distToOrigin == o.m_distToOrigin && m_lifeTime == o.m_lifeTime);
}

std::ostream &
operator<< (std::ostream & os, RrepHeader const & h)
{
  h.Print (os);
  return os;
}

//-----------------------------------------------------------------------------
// RREP-ACK
//-----------------------------------------------------------------------------

RrepAckHeader::RrepAckHeader ()
  : m_reserved (0)
{
}

NS_OBJECT_ENSURE_REGISTERED (RrepAckHeader);

TypeId
RrepAckHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::odvr::RrepAckHeader")
    .SetParent<Header> ()
    .SetGroupName ("Aodv")
    .AddConstructor<RrepAckHeader> ()
  ;
  return tid;
}

TypeId
RrepAckHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

uint32_t
RrepAckHeader::GetSerializedSize () const
{
  return 1;
}

void
RrepAckHeader::Serialize (Buffer::Iterator i ) const
{
  i.WriteU8 (m_reserved);
}

uint32_t
RrepAckHeader::Deserialize (Buffer::Iterator start )
{
  Buffer::Iterator i = start;
  m_reserved = i.ReadU8 ();
  uint32_t dist = i.GetDistanceFrom (start);
  NS_ASSERT (dist == GetSerializedSize ());
  return dist;
}

void
RrepAckHeader::Print (std::ostream &os ) const
{
}

bool
RrepAckHeader::operator== (RrepAckHeader const & o ) const
{
  return m_reserved == o.m_reserved;
}

std::ostream &
operator<< (std::ostream & os, RrepAckHeader const & h )
{
  h.Print (os);
  return os;
}

} // ODVR
} // NS3

