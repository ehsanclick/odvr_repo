/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2018 UCSC
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 * Author: Ehsan Hemmati <ehsan@ehsan.click>
 * 			 <ehemmati@ucsc.edu>
 *
 */

#include "odvr-ptable.h"
#include <algorithm>
#include <iomanip>
#include "ns3/simulator.h"
#include "ns3/log.h"


namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("OdvrPendingTable");

namespace odvr {

PrnEntry::PrnEntry (uint8_t dist, uint8_t refDist, Time lt)
  :
    m_dist (dist),
    m_refDist (refDist),
    m_lifeTime (lt + Simulator::Now ())
{
}
///ReqListEntry
/*
 *
 */
ReqListEntry::ReqListEntry ()
{
  NS_LOG_FUNCTION (this);
}

ReqListEntry::~ReqListEntry ()
{
}

/**
 *
 */
bool
ReqListEntry::AddReqEntry (Ipv4Address neighbor, PrnEntry prnEntry)
{
  NS_LOG_FUNCTION (this);
  Purge ();
  std::pair<std::map<Ipv4Address, PrnEntry>::iterator, bool> result =
    m_prn.insert (std::make_pair (neighbor, prnEntry));
  return result.second;
}
/**
 *
 */
bool
ReqListEntry::LookupReqEntry (Ipv4Address neighbor, PrnEntry & prnEntry)
{
  NS_LOG_FUNCTION (this);
  if (m_prn.empty ())
    {
      NS_LOG_DEBUG ("Pending Request List is empty");
      return false;
    }
  std::map<Ipv4Address, PrnEntry>::iterator it = m_prn.find (neighbor);
  if (it == m_prn.end ())
    {
      NS_LOG_DEBUG ("No Prn Entry for: " << neighbor );
      return false;
    }
  NS_LOG_DEBUG ("Found Entry for: " << neighbor);
  prnEntry = it->second;
  return true;
}
/**
 *
 *
 */
bool
ReqListEntry::LookupValidReqEntry (Ipv4Address neighbor, PrnEntry & prnEntry)
{
  return LookupReqEntry (neighbor, prnEntry);
}
/**
 *
 */
bool
ReqListEntry::UpdateReqEntry (Ipv4Address neighbor, PrnEntry & prnEntry)
{
  NS_LOG_FUNCTION (this);
  if (m_prn.empty ())
    {
      NS_LOG_DEBUG ("Pending Request List is empty");
      return false;
    }
  std::map<Ipv4Address, PrnEntry>::iterator it = m_prn.find (neighbor);
  if (it == m_prn.end ())
    {
      NS_LOG_DEBUG ("No Prn Entry for: " << neighbor );
      return false;
    }
  NS_LOG_DEBUG ("Found Entry for: " << neighbor);
  it->second = prnEntry;
  return true;
}
/**
 *
 */
void
ReqListEntry::Purge ()
{
  NS_LOG_FUNCTION (this);
  if (m_prn.empty ())
    {
      return;
    }
  for (std::map<Ipv4Address, PrnEntry>::iterator it = m_prn.begin (); it != m_prn.end ();)
    {
      if (it->second.GetLifeTime () < Time (0))
	{
	  std::map<Ipv4Address, PrnEntry>::iterator tmp = it;
	  ++it;
	  m_prn.erase (tmp);
	}
      else
	{
	  ++it;
	}
    }
}

/// PendingTableEntry
/**
 *
 */
PendingTableEntry::PendingTableEntry (uint8_t dist, Time lifetime)
  :
      m_refDist (dist),
      m_lifeTime (lifetime)
{
}

PendingTableEntry::~PendingTableEntry ()
{
}

/*
 *
 *
 */
bool
PendingTableEntry::AddReqList (Ipv4Address org, ReqListEntry & r)
{
  NS_LOG_FUNCTION (this);
  std::map<Ipv4Address, ReqListEntry>::iterator it = m_requestList.find (org);
  if (it == m_requestList.end ())
    {
      NS_LOG_DEBUG ("Adding new entry for: " << org);
      std::pair<std::map<Ipv4Address, ReqListEntry>::iterator, bool> result=
	  m_requestList.insert (std::make_pair (org, r));
      return result.second;
    }
  it->second.Purge ();
  if (it->second.Empty ())
    {
      NS_LOG_DEBUG ("Adding new entry for: " << org);
      it->second = r;
      return true;
    }
  NS_LOG_DEBUG ("Already has an entry for " << org);
  return false;
}

bool
PendingTableEntry::Update (Ipv4Address org, ReqListEntry & r)
{
  NS_LOG_FUNCTION (this);
  std::map<Ipv4Address, ReqListEntry>::iterator it = m_requestList.find (org);
    if (it == m_requestList.end ())
      {
	NS_LOG_LOGIC ("Entry does not exists");
	return false;
      }
    else
      {
	it->second = r;
      }
    return true;
}

bool
PendingTableEntry::LookupReqList (Ipv4Address org, ReqListEntry & pte)
{
  NS_LOG_FUNCTION (this);
  if (m_requestList.empty ())
    {
      return false;
    }
  std::map<Ipv4Address, ReqListEntry>::iterator it = m_requestList.find (org);
  if (it == m_requestList.end())
    {
      return false;
    }
  it->second.Purge ();
  if (it->second.Empty ())
    {
      m_requestList.erase(it);
      return false;
    }
  pte = it->second;
  return true;
}
/**
 *
 */
bool
PendingTableEntry::LookupValidReqList (Ipv4Address org, ReqListEntry & pte)
{
  return LookupReqList (org, pte);
}
/**
 *
 */
bool
PendingTableEntry::DeleteReqList (Ipv4Address org)
{
  return m_requestList.erase (org) > 0;
}

void
PendingTableEntry::Purge ()
{
  if (m_requestList.empty ())
    {
      return;
    }
  for (std::map<Ipv4Address, ReqListEntry>::iterator it = m_requestList.begin (); it != m_requestList.end ();)
    {
      it->second.Purge ();
      if (it->second.Empty ())
	{
	  std::map<Ipv4Address, ReqListEntry>::iterator tmp;
	  tmp = it;
	  ++it;
	  m_requestList.erase (tmp);
	}
      else
	{
	  ++it;
	}
    }
}

void
PendingTableEntry::Print (std::ostream & os) const
{
  os << "\t RefDist: " << static_cast<unsigned>(m_refDist) << " LifeTime: " << m_lifeTime;
}

/// PendingTable
/**
 *
 */
PendingTable::PendingTable (Time t)
  : m_badLinkLifetime (t)
{
}
/**
 *
 */
PendingTable::~PendingTable ()
{
}
/**
 *
 */
bool
PendingTable::AddEntry (Ipv4Address dst, PendingTableEntry & p)
{
  std::map<Ipv4Address, PendingTableEntry>::iterator it = m_ipv4AddressEntry.find (dst);
  if (it == m_ipv4AddressEntry.end ())
    {
      std::pair<std::map<Ipv4Address, PendingTableEntry>::iterator, bool> result =
        m_ipv4AddressEntry.insert (std::make_pair (dst, p));
      return result.second;
    }
  it->second.Purge ();
  if (it->second.Empty ())
    {
      it->second = p;
      return true;
    }
  else
    {
      return false;
    }
}

bool
PendingTable::Update (Ipv4Address dst, PendingTableEntry & p)
{
  NS_LOG_DEBUG (this << dst << p);
  std::map<Ipv4Address, PendingTableEntry>::iterator it = m_ipv4AddressEntry.find (dst);
  if (it == m_ipv4AddressEntry.end ())
    {
      NS_LOG_ERROR ("Couldn't find Entry for " << dst << " to update");
      return false;
    }
  else
    {
      it->second = p;
    }
  return true;

}

bool
PendingTable::DeleteEntry (Ipv4Address dst, Ipv4Address orig)
{
  if (m_ipv4AddressEntry.empty ())
    {
      return false;
    }
  std::map<Ipv4Address, PendingTableEntry>::iterator it = m_ipv4AddressEntry.find (dst);
  if (it == m_ipv4AddressEntry.end ())
    {
      return false;
    }
  return it->second.DeleteReqList (orig);
}
/**
 *
 */
bool
PendingTable::LookupEntry (Ipv4Address dst, PendingTableEntry & pte)
{
  if (m_ipv4AddressEntry.empty ())
    {
      return false;
    }
  std::map<Ipv4Address, PendingTableEntry>::iterator it = m_ipv4AddressEntry.find (dst);
  if (it == m_ipv4AddressEntry.end ())
    {
      return false;
    }
  it->second.Purge ();
  if (it->second.Empty ())
    {
      return false;
    }
  pte = it->second;
  return true;
}
/**
 *
 */
bool
PendingTable::LookupValidEntry (Ipv4Address dst, PendingTableEntry & pte)
{
  return LookupEntry (dst, pte);
}

std::ostream& operator<< (std::ostream& os, PendingTableEntry const& pte)
{
  pte.Print (os);
  return os;
}



}// ODVR
}// NS3
