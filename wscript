## -*- Mode: python; py-indent-offset: 4; indent-tabs-mode: nil; coding: utf-8; -*-

def build(bld):
    module = bld.create_ns3_module('odvr', ['internet', 'wifi'])
    module.includes = '.'
    module.source = [
        'model/odvr-id-cache.cc',
        'model/odvr-dpd.cc',
        'model/odvr-rtable.cc',
        'model/odvr-dtable.cc',
        'model/odvr-ptable.cc',
        'model/odvr-rqueue.cc',
        'model/odvr-packet.cc',
        'model/odvr-neighbor.cc',
        'model/odvr-routing-protocol.cc',
        'helper/odvr-helper.cc',
        ]

    odvr_test = bld.create_ns3_module_test_library('odvr')
    odvr_test.source = [
        'test/odvr-id-cache-test-suite.cc',
        'test/odvr-test-suite.cc',
        'test/odvr-regression.cc',
        'test/bug-772.cc',
        'test/loopback.cc',
        ]

    headers = bld(features='ns3header')
    headers.module = 'odvr'
    headers.source = [
        'model/odvr-id-cache.h',
        'model/odvr-dpd.h',
        'model/odvr-rtable.h',
        'model/odvr-rqueue.h',
        'model/odvr-ptable.h',
        'model/odvr-dtable.h',
        'model/odvr-packet.h',
        'model/odvr-neighbor.h',
        'model/odvr-routing-protocol.h',
        'helper/odvr-helper.h',
        ]

    if bld.env['ENABLE_EXAMPLES']:
        bld.recurse('examples')

    bld.ns3_python_bindings()
